//
//  NewsGeneratorTests.swift
//  NewsGeneratorTests
//
//  Created by Macbook Pro on 05/09/2018.
//  Copyright © 2018 stefanoshea. All rights reserved.
//

import XCTest
@testable import NewsGenerator

class NewsGeneratorTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testConnectedToInternet() {
        let isConnected = Reachability.isConnectedToNetwork()
        XCTAssertTrue(isConnected, "Not connected to internet")
    }
    
    func testImageDownload() {
        
        let cellType = CellType.random()
        let source   = Source(id: "1", name: "Source", description: "Test Source", url: "https://www.google.com")
        
        let testArticle = Article(cellType: cellType,
                                  containsAustralia: true,
                                  source: source,
                                  author: "Tester",
                                  publishedAt: "20180101",
                                  title: "Breaking News: Tests Pass",
                                  description: "Test description",
                                  url: "https://www.google.com/news",
                                  urlToImage: "https://placehold.it/120x120&text=image1")
        
        let apiImageView = APIImageView()
        apiImageView.downloadedFromLink(testArticle.urlToImage!)
        
        XCTAssertFalse(apiImageView.image == nil, "Error")
    }
    
    func testAPIsource() {
        
        let apiExpectation = expectation(description: "API fetch Complete")
        
        let service     = APIService()
        service.getSources({ error, source in
            
            let sourceCount = source?.sources.count ?? 0
            XCTAssertTrue(error == nil, (error?.localizedDescription)!)
            XCTAssertTrue(sourceCount > 0, "No sources returned")
            
            apiExpectation.fulfill()
        })
        
        waitForExpectations(timeout: 20.0, handler: { error in
            if let error = error {
                XCTFail("Timeout Error: \(error.localizedDescription)")
            }
        })
    }
}
