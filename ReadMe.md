#Beem It - iOS Coding Exercise


This app was created by **Stefan O'Shea** as part of the interview process for a role with Beem It.

##App Notes
For the purposes of this exercise, the app is optimised for iPhone. Although it works on iPad, I have not implemented any size classes or something like a split view controller so the user experience is only ok on iPad.

The API Service class has 2 hard coded requests for the purposes of this exercise. I would generally take a more generic approach to this kind of request but since there were 2 very specific instructions here, I have hard coded the request for better performance.

The Google News AU API isn't the most reliable. It quite often returns no image URL. I set the image to a generic blank image when this happens.

In my initial design, the BigImageTableViewCell filled the cell entirely which I think was a nice looking design. However, in order to show the gold backround of the cell when it contains `Australia` or `Australian`, I have reduced the size of the image inside the cell.

Another note on this requirement; I check the Title and Description for `Australia` and `Australian` so sometimes the cell will be gold because it matches the description. This tends to happen with articles from The Australian Financial Review because they tend to have their name in the description. So the cell will be gold even though the article isn't neccessarily about Australia.

I would usually use the [Toast](https://github.com/scalessec/Toast-Swift) framework to display a loading icon in appropriate places, such as when the tableview or webview are loading, to give a better user experience but the requirements state no 3rd party frameworks so I have just used Apple's UIActivityIndicatorView.

##Testing
I have written some unit tests to test the api functionality.

I have also done pretty extensive manual testing myself on a couple of devices. I haven't encountered any crashes and performance seems to be good.

##Resources
APIImageView is based on the answer on StackOverflow to [this question](https://stackoverflow.com/questions/24231680/loading-downloading-image-from-url-on-swift).

Reachability is based on the answer on StackOverflow to [this question](https://stackoverflow.com/questions/30743408/check-for-internet-connection-with-swift). 


### Contact
Email: <stefanoshea@gmail.com>   
LinkedIn: <http://www.linkedin.com/in/stefanoshea> 
  
   
Phone: 0452646717