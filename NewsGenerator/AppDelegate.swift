//
//  AppDelegate.swift
//  NewsGenerator
//
//  Created by Macbook Pro on 05/09/2018.
//  Copyright © 2018 stefanoshea. All rights reserved.
//

import UIKit

var newsSource: NewsSource!
let gNavyColor = UIColor(red: 0.0, green: 38/255, blue: 67/255, alpha: 1)
let gGoldColor = UIColor(red:0.95, green:0.79, blue:0.15, alpha:1.0)

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var navigationController: UINavigationController?
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        preloadData()
        
        setupAppearance(application)
        
        return true
    }
    
    func setupAppearance(_ application: UIApplication) -> Void {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        
        if let window = window {
            
            let viewController  = ArticleListViewController()
            viewController.view.backgroundColor = .white
            navigationController   = UINavigationController(rootViewController: viewController)
            
            window.rootViewController = navigationController
            window.makeKeyAndVisible()
        }
        
        UINavigationBar.appearance().isTranslucent  = false
        UINavigationBar.appearance().barTintColor   = gNavyColor
        UINavigationBar.appearance().tintColor      = .white
        
        UINavigationBar.appearance().barStyle       = .black
        application.statusBarStyle                  = .lightContent
        
        let attrs = [
            NSAttributedStringKey.font: UIFont(name: "AmericanTypewriter", size: 18)!
        ]
        
        UINavigationBar.appearance().titleTextAttributes = attrs
    }
    
    //Preload the sources data
    func preloadData() {
        let service     = APIService()
        service.getSources({ error, source in
            newsSource = source
        })
    }
}

