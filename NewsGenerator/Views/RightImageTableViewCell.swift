//
//  TableViewCellOne.swift
//  BeemDemo
//
//  Created by Macbook Pro on 05/09/2018.
//  Copyright © 2018 stefanoshea. All rights reserved.
//

import UIKit

//Image on the right, text on the left
class RightImageTableViewCell : UITableViewCell {
    
    var article: Article? {
        didSet {
            guard let article = article else {
                return
            }
            articleTitle.text   = article.title
            articleSource.text  = "Source: \(article.source.name)"
            publishedAt.text      = String(article.publishedAt.prefix(10))
            
            let url = article.urlToImage ?? ""
            articleImage.downloadedFromLink(url)
            
            //Bonus requirement 2
            if article.containsAustralia {
                backgroundColor = gGoldColor
            }
        }
    }
    
    var articleImage: APIImageView!
    var containerView: UIView!
    var articleTitle: UILabel!
    var articleSource: UILabel!
    var publishedAt: UILabel!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        articleImage    = APIImageView()
        articleTitle    = UILabel()
        articleSource   = UILabel()
        publishedAt       = UILabel()
        containerView   = UIView()
        
        articleTitle.translatesAutoresizingMaskIntoConstraints  = false
        articleImage.translatesAutoresizingMaskIntoConstraints  = false
        articleSource.translatesAutoresizingMaskIntoConstraints = false
        publishedAt.translatesAutoresizingMaskIntoConstraints     = false
        containerView.translatesAutoresizingMaskIntoConstraints = false
        
        contentView.addSubview(containerView)
        contentView.addSubview(articleImage)
        contentView.addSubview(articleTitle)
        contentView.addSubview(articleSource)
        contentView.addSubview(publishedAt)
        
        backgroundColor = .lightText
        
        styleViews()
        setupConstraints()
    }
    
    func styleViews() {
        
        //article title label
        articleTitle.font           = UIFont(name: "AmericanTypewriter", size: 14.0)
        articleTitle.textColor      = UIColor.black.withAlphaComponent(0.9)
        articleTitle.textAlignment  = .left
        articleTitle?.numberOfLines = 0
        articleTitle.sizeToFit()
        
        //article source label
        articleSource.font      = UIFont(name: "AmericanTypewriter", size: 10.0)
        articleSource.textColor = UIColor.lightGray
        articleSource.sizeToFit()
        articleSource.textAlignment = .right
        
        //date added label
        publishedAt.font      = UIFont(name: "AmericanTypewriter", size: 10.0)
        publishedAt.textColor = UIColor.lightGray
        publishedAt.textAlignment = .right
        
        //article imageview
        articleImage.clipsToBounds = true
        
        //container view
        containerView.layer.cornerRadius    = 5.0
        containerView.backgroundColor = .white
    }
    
    func setupConstraints() {
        //container view constraints
        NSLayoutConstraint.activate([
            containerView.leftAnchor.constraint(equalTo: contentView.leftAnchor,
                                                constant: 8.0),
            
            containerView.topAnchor.constraint(equalTo: contentView.topAnchor,
                                               constant: 8),
            
            containerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor,
                                                  constant: -8),
            
            containerView.rightAnchor.constraint(equalTo: contentView.rightAnchor,
                                                 constant: -8)
            ])
        
        //article imageview constraints
        NSLayoutConstraint.activate([
            articleImage.rightAnchor.constraint(equalTo: containerView.rightAnchor,
                                                constant: -8),
            articleImage.topAnchor.constraint(equalTo: containerView.topAnchor,
                                              constant: 0),
            articleImage.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
            articleImage.widthAnchor.constraint(equalTo: containerView.widthAnchor,
                                                multiplier: 0.3),
            articleImage.heightAnchor.constraint(equalToConstant: 100)
            ])
        
        //article title label constraints
        NSLayoutConstraint.activate([
            articleTitle.leftAnchor.constraint(equalTo: containerView.leftAnchor,
                                               constant: 8),
            articleTitle.rightAnchor.constraint(equalTo: articleImage.leftAnchor,
                                                constant: -8),
            articleTitle.topAnchor.constraint(equalTo: articleImage.topAnchor,
                                              constant: 8)
            ])
        
        //date added label constraints
        NSLayoutConstraint.activate([
            publishedAt.bottomAnchor.constraint(equalTo: containerView.bottomAnchor,
                                              constant: -4),
            publishedAt.leftAnchor.constraint(equalTo: containerView.leftAnchor,
                                            constant: 8),
            publishedAt.rightAnchor.constraint(equalTo: articleImage.leftAnchor,
                                             constant: -8)
            ])
        
        //article source constraints
        NSLayoutConstraint.activate([
            articleSource.bottomAnchor.constraint(equalTo: publishedAt.topAnchor),
            articleSource.leftAnchor.constraint(equalTo: containerView.leftAnchor,
                                                constant: 8),
            articleSource.rightAnchor.constraint(equalTo: articleImage.leftAnchor,
                                                 constant: -8)
            ])
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
