//
//  TableViewCellOne.swift
//  BeemDemo
//
//  Created by Macbook Pro on 05/09/2018.
//  Copyright © 2018 stefanoshea. All rights reserved.
//

import UIKit

//image on the left, text on the right
class LeftImageTableViewCell : RightImageTableViewCell {
    
    override func setupConstraints() {
        //container view
        NSLayoutConstraint.activate([
            containerView.leftAnchor.constraint(equalTo: contentView.leftAnchor,
                                                constant: 8.0),
            containerView.topAnchor.constraint(equalTo: contentView.topAnchor,
                                               constant: 8),
            containerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor,
                                                  constant: -8),
            containerView.rightAnchor.constraint(equalTo: contentView.rightAnchor,
                                                 constant: -8)
            ])
        
        //article imageview
        NSLayoutConstraint.activate([
            articleImage.leftAnchor.constraint(equalTo: containerView.leftAnchor,
                                               constant: 8),
            articleImage.topAnchor.constraint(equalTo: containerView.topAnchor),
            articleImage.bottomAnchor.constraint(equalTo: containerView.bottomAnchor),
            articleImage.widthAnchor.constraint(equalTo: containerView.widthAnchor,
                                                multiplier: 0.3),
            articleImage.heightAnchor.constraint(equalToConstant: 100)
            ])
        
        //article title label
        NSLayoutConstraint.activate([
            articleTitle.leftAnchor.constraint(equalTo: articleImage.rightAnchor,
                                               constant: 8),
            articleTitle.rightAnchor.constraint(equalTo: containerView.rightAnchor,
                                                constant: -8),
            articleTitle.topAnchor.constraint(equalTo: articleImage.topAnchor,
                                              constant: 8.0)
            ])
        
        //date added label
        NSLayoutConstraint.activate([
            publishedAt.bottomAnchor.constraint(equalTo: containerView.bottomAnchor,
                                              constant: -4),
            publishedAt.leftAnchor.constraint(equalTo: articleImage.rightAnchor,
                                            constant: 8),
            publishedAt.rightAnchor.constraint(equalTo: containerView.rightAnchor,
                                             constant: -8)
            ])
        
        
        //article source label
        NSLayoutConstraint.activate([
            articleSource.bottomAnchor.constraint(equalTo: publishedAt.topAnchor),
            articleSource.leftAnchor.constraint(equalTo: articleImage.rightAnchor,
                                                constant: 8),
            articleSource.rightAnchor.constraint(equalTo: containerView.rightAnchor,
                                                 constant: -8)
            ])
    }
}
