//
//  CellThreeImageView.swift
//  BeemDemo
//
//  Created by Macbook Pro on 05/09/2018.
//  Copyright © 2018 stefanoshea. All rights reserved.
//

import UIKit

//Use cache to ensure smooth scrolling and no image load issues
let imageCache = NSCache<AnyObject, AnyObject>()

class APIImageView: UIImageView {
    
    var imageUrlString: String?
    
    func downloadedFromLink(_ urlString: String, contentMode mode: UIViewContentMode = UIViewContentMode.scaleAspectFill) {
        
        guard let url = URL(string: urlString) else {
            //No image url, set blank photo as image
            self.image  = #imageLiteral(resourceName: "blankPhoto")
            contentMode = .scaleAspectFit
            
            return }
        
        imageUrlString = urlString
        
        self.image = UIImage()
        contentMode = mode
        
        //Check if we have the image in our cache
        if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            self.image = imageFromCache
            return
        }
        
        
        URLSession.shared.dataTask(with: url, completionHandler: { (data, response, error) in
            
            if let data = data, let image = UIImage(data: data) {
                DispatchQueue.main.async(execute: {
                    let imageToCache = image
                    
                    //Cache image
                    if self.imageUrlString == urlString {
                        self.image = imageToCache
                    } 
                    imageCache.setObject(imageToCache, forKey: urlString as AnyObject)
                    })
            }
            
        }).resume()
    }
}
