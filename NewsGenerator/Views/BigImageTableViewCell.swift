//
//  TableViewCellOne.swift
//  BeemDemo
//
//  Created by Macbook Pro on 05/09/2018.
//  Copyright © 2018 stefanoshea. All rights reserved.
//

import UIKit

//large image with text
class BigImageTableViewCell: RightImageTableViewCell {

    override func styleViews() {
  
        //Article Title label
        articleTitle.textColor      = .white
        articleTitle.shadowColor    = .black
        articleTitle.numberOfLines  = 0
        articleTitle.textAlignment  = .center
        articleTitle.font           = UIFont(name: "AmericanTypewriter", size: 15.0)
        articleTitle.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        //Date added label
        publishedAt.textAlignment = .center
        publishedAt.textColor     = .white
        publishedAt.shadowColor   = .black
        publishedAt.font          = UIFont(name: "AmericanTypewriter", size: 10.0)
        publishedAt.backgroundColor = UIColor.black.withAlphaComponent(0.5)

        //Article imageview
        articleImage.clipsToBounds   = true
        
        //Article source label
        articleSource.textColor     = .white
        articleSource.textAlignment = .center
        articleSource.shadowColor   = .black
        articleSource.font          = UIFont(name: "AmericanTypewriter", size: 10.0)
        articleSource.backgroundColor = UIColor.black.withAlphaComponent(0.5)
  
    }
    
    override func setupConstraints() {
  
       //Article imageview constraints
        NSLayoutConstraint.activate([
            articleImage.topAnchor.constraint(equalTo: contentView.topAnchor,
                                              constant: 8.0),
            articleImage.bottomAnchor.constraint(equalTo: contentView.bottomAnchor,
                                                 constant: -8.0),
            articleImage.leftAnchor.constraint(equalTo: contentView.leftAnchor,
                                               constant: 8.0),
            articleImage.rightAnchor.constraint(equalTo: contentView.rightAnchor,
                                                constant: -8.0),
            articleImage.heightAnchor.constraint(equalToConstant: 220)
            ])
        
        //Article title label constraints
        NSLayoutConstraint.activate([
            articleTitle.bottomAnchor.constraint(equalTo: articleImage.bottomAnchor),
            articleTitle.leftAnchor.constraint(equalTo: articleImage.leftAnchor),
            articleTitle.rightAnchor.constraint(equalTo: articleImage.rightAnchor),
            articleTitle.heightAnchor.constraint(equalTo: articleImage.heightAnchor,
                                                 multiplier: 0.25)
            ])
        
        //Article source label constraints
        NSLayoutConstraint.activate([
            articleSource.topAnchor.constraint(equalTo: articleImage.topAnchor),
            articleSource.leftAnchor.constraint(equalTo: articleImage.leftAnchor),
            articleSource.widthAnchor.constraint(equalTo: articleImage.widthAnchor,
                                                 multiplier: 0.5),
            articleSource.heightAnchor.constraint(equalTo: articleImage.heightAnchor,
                                                  multiplier: 0.1)
            ])
        
        //Date added label constraints
        NSLayoutConstraint.activate([
            publishedAt.topAnchor.constraint(equalTo: articleImage.topAnchor),
            publishedAt.rightAnchor.constraint(equalTo: articleImage.rightAnchor),
            publishedAt.widthAnchor.constraint(equalTo: articleImage.widthAnchor,
                                             multiplier: 0.5),
            publishedAt.heightAnchor.constraint(equalTo: articleImage.heightAnchor,
                                              multiplier: 0.1)
            ])
    }
}
