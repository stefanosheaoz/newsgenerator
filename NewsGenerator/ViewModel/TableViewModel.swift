//
//  TableViewModel.swift
//  BeemDemo
//
//  Created by Macbook Pro on 06/09/2018.
//  Copyright © 2018 stefanoshea. All rights reserved.
//

import UIKit

enum CellType: UInt32 {
    case Right
    case Left
    case BigImage
    
    // Have 3 different styles of TableViewCell picked at random
    static func random() -> CellType {
        let maxValue = BigImage.rawValue
        
        let rand = arc4random_uniform(maxValue+1)
        return CellType(rawValue: rand)!
    }
}

class TableViewModel: NSObject {
    
    weak var delegate: TableViewModelDelegate?
    weak var timer: Timer!
    
    var cellHeightsDictionary: [IndexPath: CGFloat] = [:]
    
    var articles =  [Article]()
    
    func startLoadingData() {

        if newsSource == nil {
            let service     = APIService()
            service.getSources({ error, source in
                newsSource = source
            })
        }
        
        if timer != nil {
            timer.invalidate()
        }
        
        //Article generated at a random interval of 1-6 seconds
        let randomNumber = Double(arc4random_uniform(6) + 1)
        let dispatchTime = DispatchTime.now() + randomNumber
        
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
            self.timer = Timer.scheduledTimer(timeInterval: 1.0,
                                              target: self,
                                              selector: #selector(self.getArticle),
                                              userInfo: nil,
                                              repeats: false)
            
            self.timer.fire()
            
            //Timer will be invalidated and a new timer will start
            self.startLoadingData()
        })
    }
    
    
    @objc func getArticle() {
        //Check internet connection
        guard Reachability.isConnectedToNetwork() else {
            return
        }
        
        //Get a randomly selected article from random source
        APIService().getRandomArticle(completion: { article in
            
            //Check if we already have this article and don't display if we do
            if !self.articles.contains(article) {
                self.articles.append(article)
                self.delegate?.didFinishUpdates()
            }
        })
    }
}

extension TableViewModel: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let article  = articles[indexPath.row]
       
        switch article.cellType! {
        case .Left:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "leftCell", for: indexPath) as? LeftImageTableViewCell {
                cell.article = article
                
                return cell
            }
        case .Right:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "rightCell", for: indexPath) as? RightImageTableViewCell {
                cell.article = article
                
                return cell
            }
        case .BigImage:
            if let cell = tableView.dequeueReusableCell(withIdentifier: "bigImageCell", for: indexPath) as? BigImageTableViewCell {
                cell.article = article
                
                return cell
            }
        }
        
        return UITableViewCell()
    }
}

extension TableViewModel: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedArticle = articles[indexPath.row]
        delegate?.articleListViewControllerDidSelectArticle(selectedArticle)
        
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    //Ensures smooth scroll when loading data
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        self.cellHeightsDictionary[indexPath] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if let height =  self.cellHeightsDictionary[indexPath] {
            return height
        }
        return UITableViewAutomaticDimension
    }
}


protocol TableViewModelDelegate: class {
    func didFinishUpdates()
    func articleListViewControllerDidSelectArticle(_ selectedArticle: Article)
}
