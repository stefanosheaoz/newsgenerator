//
//  APIService.swift
//  BeemDemo
//
//  Created by Macbook Pro on 05/09/2018.
//  Copyright © 2018 stefanoshea. All rights reserved.
//

import UIKit

public class APIService {
    
    static let session      = URLSession.shared
    private let sourceURL   = URL(string: "newsapi.org/v2/")!
    
    let apiKey     = "bc5ef0fc481e4b84ad38db226bddc5dc"
    
    var headers: [String:String]
    
    required public init() {
        headers = [String:String]()
        
        headers["Content-Type"]     = "application/json"
        headers["Accept"]            = "*/*"
    }
    
    
    func getSources(_ completion: @escaping (Error?, NewsSource?) -> Void) -> Void {

        //Call the 'sources' endpoint
        var request        = URLRequest(url: URL(string: "https://\(sourceURL)sources?language=en&country=au&apiKey=\(apiKey)")!,
                                        cachePolicy: .useProtocolCachePolicy,
                                        timeoutInterval: 30.0)
        
        request.httpMethod          = "GET"
        request.allHTTPHeaderFields = headers
        
        APIService.session.dataTask(with: request) { (data, response, error) in
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode < 200 || httpResponse.statusCode > 399 {
                    let serverMsg        = HTTPURLResponse.localizedString(forStatusCode: httpResponse.statusCode)
                    
                    completion(NSError(domain: Bundle.main.bundleIdentifier!,
                                       code: httpResponse.statusCode,
                                       userInfo: [NSLocalizedDescriptionKey: serverMsg]), nil)
                    
                    return
                } else {
                    do {
                        if let data = data {
                            let decoder = JSONDecoder()
                            newsSource = try decoder.decode(NewsSource.self, from: data)
                            
                            completion(error, newsSource)
                        }
                    } catch let err {
                        print("error loading data", err)
                    }
                }
            }
        }.resume()
    }
    
    func getRandomArticle(completion: @escaping (Article) -> Void) -> Void {
        guard newsSource != nil else { return }
        
        // Select random source
        let sourcesCount        = UInt32(newsSource.sources.count)
        let randomSourceIndex   = Int(arc4random_uniform(sourcesCount))
        let randomSource        = newsSource.sources[randomSourceIndex].id
        
        //Call the 'everything' endpoint
        var request = URLRequest(url: URL(string: "https://\(sourceURL)everything?language=en&sources=\(randomSource)&apiKey=\(apiKey)")!,
                                 cachePolicy: .useProtocolCachePolicy,
                                 timeoutInterval: 30.0)
        
        request.httpMethod  = "GET"
        request.allHTTPHeaderFields = headers
        
       APIService.session.dataTask(with: request) { (data, response, error) in
            if let httpResponse = response as? HTTPURLResponse {
                if httpResponse.statusCode == 200 {
                    if let data = data {
                        let decoder = JSONDecoder()
                        
                        let returnedArticles = try? decoder.decode(NewsResult.self, from: data)
                        
                        //Select random article
                        let articleCount  = UInt32(returnedArticles!.articles.count)
                        let randomIndex   = Int(arc4random_uniform(articleCount))
                        var randomArticle = returnedArticles!.articles[randomIndex]
                        
                        //Fill our custom properties
                        randomArticle.checkContainsAustralia()
                        randomArticle.generateCellType()
                        completion(randomArticle)
                    }
                }
            }
        }.resume()
    }
}
