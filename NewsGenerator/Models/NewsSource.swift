//
//  NewsSource.swift
//  BeemDemo
//
//  Created by Macbook Pro on 05/09/2018.
//  Copyright © 2018 stefanoshea. All rights reserved.
//

import Foundation

struct NewsSource: Codable {
    let sources: [Source]
}

struct Source: Codable {
    let id, name: String
    let description, url: String?
}

struct NewsResult: Codable {
    let articles: [Article]
}
