//
//  Article.swift
//  BeemDemo
//
//  Created by Macbook Pro on 05/09/2018.
//  Copyright © 2018 stefanoshea. All rights reserved.
//

import Foundation

struct Article: Codable {
    
    // Custom Properties
    var cellType: CellType?     = nil
    var containsAustralia: Bool = false
    
    //Codable Properties
    let source: Source
    let author: String?
    let publishedAt: String
    let title, description: String
    let url, urlToImage: String?
    
    enum CodingKeys: String, CodingKey {
        case source, author, publishedAt, title, description, url, urlToImage
    }
}

extension Article: Hashable {
    
    //Conform to Hashable so we can use .contains
    static func == (lhs: Article, rhs: Article) -> Bool {
        return lhs.title == rhs.title
    }
    var hashValue: Int {
        return title.hashValue
    }
    
    //Check if the article contains Australia
    mutating func checkContainsAustralia(){
        if title.contains("Austalia") || title.contains("Australian") || description.contains("Australia") || description.contains("Australian") {
            containsAustralia = true
        }
    }
    
    //Generate random cell type
    mutating func generateCellType() {
        cellType = CellType.random()
    }
}
