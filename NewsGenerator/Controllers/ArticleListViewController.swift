//
//  ArticleListViewController.Swift
//  BeemDemo
//
//  Created by Macbook Pro on 05/09/2018.
//  Copyright © 2018 stefanoshea. All rights reserved.
//

import UIKit
import Foundation

class ArticleListViewController: UIViewController, UITableViewDelegate {
    weak var delegate: TableViewModelDelegate?
    
    let viewModel = TableViewModel()
    
    var tableView: UITableView!
    var activityIndicatorView: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Latest News"
        
        activityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        
        tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.backgroundView = activityIndicatorView
        
        tableView.delegate      = viewModel
        tableView.dataSource    = viewModel
        viewModel.delegate      = self
        
        //Register cells
        tableView.register(LeftImageTableViewCell.self, forCellReuseIdentifier: "leftCell")
        tableView.register(RightImageTableViewCell.self, forCellReuseIdentifier: "rightCell")
        tableView.register(BigImageTableViewCell.self, forCellReuseIdentifier: "bigImageCell")
        
        tableView.separatorStyle     = .none
        tableView.estimatedRowHeight = 200
        tableView.rowHeight          = UITableViewAutomaticDimension
        tableView.backgroundColor    = UIColor.black.withAlphaComponent(0.2)
        
        
        //Refresh bar button item
        let refresh = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(loadData))
        
        navigationItem.rightBarButtonItem = refresh
        
        view.addSubview(tableView)
        
        NSLayoutConstraint.activate([
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            tableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            tableView.rightAnchor.constraint(equalTo: view.rightAnchor)
            ])
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //Check internet connection
        guard Reachability.isConnectedToNetwork() else {
            AlertControlManager.showInternetConnectionErrorAlert(forTarget: self)
            
            return
        }
        loadData()
    }
    
    @objc func loadData() {
        
        //Only load data if table is empty
        if tableView.numberOfRows(inSection: 0) == 0 {
            activityIndicatorView.startAnimating()
            viewModel.startLoadingData()
        }
    }
}

extension ArticleListViewController: TableViewModelDelegate {
    
    func articleListViewControllerDidSelectArticle(_ selectedArticle: Article) {
        let detailVC                = DetailViewController()
        detailVC.selectedArticle    = selectedArticle
        
        guard Reachability.isConnectedToNetwork() else {
            AlertControlManager.showInternetConnectionErrorAlert(forTarget: self)
            
            return
        }
        navigationController?.pushViewController(detailVC, animated: true)
    }
    
    func didFinishUpdates() {
         DispatchQueue.main.async {
            self.tableView.reloadData()
            self.tableView.layoutIfNeeded()
            self.activityIndicatorView.stopAnimating()
        }
    }
}
