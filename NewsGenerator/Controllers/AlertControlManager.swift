//
//  AlertControllerManager.swift
//  BeemDemo
//
//  Created by Macbook Pro on 06/09/2018.
//  Copyright © 2018 stefanoshea. All rights reserved.
//

import Foundation
import UIKit

class AlertControlManager: NSObject {
    static func showInternetConnectionErrorAlert(forTarget target: UIViewController) {
        let alertController = UIAlertController(title: "No Internet Connection", message: "No internet connection detected. Please connect to the internet and tap the refresh button to try again.", preferredStyle: .alert)
    
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertController.addAction(okAction)
        
        target.present(alertController, animated: true)
    }
}
