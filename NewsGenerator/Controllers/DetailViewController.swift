//
//  DetailViewController.swift
//  BeemDemo
//
//  Created by Macbook Pro on 06/09/2018.
//  Copyright © 2018 stefanoshea. All rights reserved.
//

import UIKit
import WebKit

class DetailViewController: UIViewController {
    
    var selectedArticle: Article!
    
    weak var webView: WKWebView!
    var titleLabel: UILabel!
    var subtitleLabel: UILabel!
    
    var activityIndicator: UIActivityIndicatorView!
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        loadWebView()
        view.backgroundColor = .white
        
    }
    
    func setupViews() {
        webView                     = WKWebView(frame: CGRect.zero)
        webView.navigationDelegate  = self
        webView.uiDelegate          = self
        
        activityIndicator           = UIActivityIndicatorView()
        activityIndicator.center    = self.view.center
        activityIndicator.hidesWhenStopped              = true
        activityIndicator.activityIndicatorViewStyle    = UIActivityIndicatorViewStyle.gray
        
        webView.addSubview(activityIndicator)
        
        titleLabel      = UILabel()
        subtitleLabel   = UILabel()
        
        view.addSubview(webView)
        view.addSubview(titleLabel)
        view.addSubview(subtitleLabel)
        
        titleLabel.font             = UIFont(name: "AmericanTypewriter-Bold", size: 20.0)
        titleLabel.textAlignment    = .center
        titleLabel.text             = selectedArticle.title
        titleLabel.numberOfLines    = 0
        
        let source      = selectedArticle.source.name
        let author      = selectedArticle.author ?? "Unknown"
        let publishedAt = selectedArticle.publishedAt
        
        subtitleLabel.textAlignment = .center
        subtitleLabel.numberOfLines = 0
        subtitleLabel.font          = UIFont(name: "AmericanTypewriter", size: 10.0)
        subtitleLabel.text          = "Author: \(author) \n \(source) - \(publishedAt)"
        
        //AutoLayout
        webView.translatesAutoresizingMaskIntoConstraints       = false
        titleLabel.translatesAutoresizingMaskIntoConstraints    = false
        subtitleLabel.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            titleLabel.leftAnchor.constraint(equalTo: view.leftAnchor),
            titleLabel.rightAnchor.constraint(equalTo: view.rightAnchor),
            titleLabel.topAnchor.constraint(equalTo: view.topAnchor, constant: 8.0)
            ])
        
        NSLayoutConstraint.activate([
            subtitleLabel.leftAnchor.constraint(equalTo: view.leftAnchor),
            subtitleLabel.rightAnchor.constraint(equalTo: view.rightAnchor),
            subtitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 4.0)
            ])
        
        NSLayoutConstraint.activate([
            webView.topAnchor.constraint(equalTo: subtitleLabel.bottomAnchor),
            webView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            webView.leftAnchor.constraint(equalTo: view.leftAnchor),
            webView.rightAnchor.constraint(equalTo: view.rightAnchor)
            ])
    }
    
    func loadWebView() {
        if let url = selectedArticle.url {
            let requestURL  = URL(string: url.replacingOccurrences(of: "http:", with: "https:"))
            let request     = URLRequest(url: requestURL!)
        
            activityIndicator.startAnimating()
            webView.load(request)
        }
    }

}

extension DetailViewController: WKNavigationDelegate, WKUIDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        activityIndicator.stopAnimating()
    }
}
